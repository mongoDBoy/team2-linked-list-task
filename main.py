from typing import Any
import functools


def check_types(func):
    @functools.wraps(func)
    def wrapper(obj, value, *args):
        if not isinstance(value, obj.nodes_type):
            raise TypeError
        return func(obj, value, *args)
    return wrapper


class EmptyNode:
    _instance = None

    def __init__(self):
        raise RuntimeError('Call instance() instead')

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls.__new__(cls)
        return cls._instance


class Node:
    def __init__(self, value: Any, next_node=EmptyNode.instance()):
        self.value = value
        self.next = next_node


class LinkedListNodeIterator:
    def __init__(self, linked_list):
        self.current_node = linked_list.head

    def __iter__(self):
        return self

    def __next__(self):
        if self.current_node == EmptyNode.instance():
            raise StopIteration

        current_node = self.current_node
        self.current_node = self.current_node.next

        return current_node


class LinkedListValueIterator(LinkedListNodeIterator):
    def __next__(self):
        return super().__next__().value


class LinkedList:
    def __init__(self, nodes_type, *values):
        self.nodes_type = nodes_type
        self.head = EmptyNode.instance()
        for arg in values[::-1]:
            self.prepend(arg)

    def get_node_by_idx(self, index):
        for i, node in enumerate(LinkedListNodeIterator(self)):
            if i == index:
                return node
        raise IndexError

    def __getitem__(self, index):
        node = self.get_node_by_idx(index)
        return node.value

    def __len__(self):
        return functools.reduce(lambda acc, x: acc + 1, self, 0)

    def __str__(self):
        return " ".join(str(node) for node in self)

    def __bool__(self):
        return self.head == EmptyNode.instance()

    def __delitem__(self, index):
        if index == 0:
            if self.head == EmptyNode.instance():
                raise IndexError
            self.head = self.head.next
            return

        node = self.get_node_by_idx(index - 1)
        prev_node, node_to_del = node, node.next

        if node_to_del == EmptyNode.instance():
            raise IndexError

        prev_node.next = node_to_del.next

    def __iter__(self):
        return LinkedListValueIterator(self)

    @check_types
    def prepend(self, value):
        if self.head == EmptyNode.instance():
            self.head = Node(value)
        else:
            self.head = Node(value, self.head)

    @check_types
    def insert(self, value, index):
        if index == 0:
            self.prepend(value)
            return

        node = self.get_node_by_idx(index - 1)
        prev_node, next_node = node, node.next
        prev_node.next = Node(value, next_node)


def main():
    my_linked_list = LinkedList(int, 0, 1, 2)

    print("Third element:")
    print(my_linked_list[2])
    print()

    print("Delete third element:")
    del my_linked_list[2]
    print(my_linked_list)
    print()

    print("Insert 13 as second element:")
    my_linked_list.insert(13, 1)
    print(my_linked_list)
    print()

    print("Add 666 to the beginning:")
    my_linked_list.prepend(666)
    print(my_linked_list)
    print()

    print("Iteration:")
    for element in LinkedListValueIterator(my_linked_list):
        print(element)
    print()

    print("Index error:")
    try:
        my_linked_list[1000]
    except IndexError:
        print("IndexError")
    print()

    print("Type error:")
    try:
        LinkedList(int, 0, "3rr0r", 2)
    except TypeError:
        print("TypeError")
    print()


if __name__ == '__main__':
    main()
